import {Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ViewEncapsulation, ElementRef} from '@angular/core';
import {ComponentRouterService} from '../../../../core/services/home/component-router.service';
import {Router} from '@angular/router';
import {ContentMenuService} from '../../services/content-menu/content-menu.service';
import {LoginService} from '../../../../core/services/auth/login.service';

/**
 * Main viewer.
 * @author Roshan Gerard Bolonna
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, OnDestroy {

  // Property "static" specification mandatory since ng8, set to "true" will create components on ngOnInit
  @ViewChild('mainView', {
    read: ViewContainerRef,
    static: true
  }) viewContainerRef: ViewContainerRef;

  protected loginState: boolean = false;

  // ****** References to subscriptions. ****** //
  private subLoginState: any;

  constructor(
    private router: Router,
    private componentRouterService: ComponentRouterService,
    private contentMenuService: ContentMenuService,
    private loginService: LoginService
  ) {
    this.initServiceSubscriptions();
  }

  ngOnInit() {
    this.componentRouterService.setRootViewContainer(this.viewContainerRef);
  }

  ngOnDestroy() {
    this.subLoginState.unsubscribe();
  }

  initServiceSubscriptions() {
    this.subLoginState = this.loginService.subLoginState.subscribe((loginState) => {
      this.loginState = loginState;
      if (this.loginState === true) {
        this.componentRouterService.renderUserRegistration(false);
      } else {
        if (this.viewContainerRef !== undefined) {
          this.componentRouterService.renderUserRegistration(true);
        }
      }
    });
  }
}
