import { Component, OnInit, OnDestroy } from '@angular/core';
import {LoginService} from '../../../../core/services/auth/login.service';

/**
 * Main menu bar.
 * @author Roshan Gerard Bolonna
 */
@Component({
  selector: 'app-content-menu',
  templateUrl: './content-menu.component.html',
  styleUrls: ['./content-menu.component.scss']
})
export class ContentMenuComponent implements OnInit, OnDestroy {

  private subLoingStateRef: any;

  protected loggedInUserName: string;

  constructor(private loginService: LoginService) {
    this.initSubscriptionServices();
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subLoingStateRef.destroy();
  }

  initSubscriptionServices() {
    this.subLoingStateRef = this.loginService.subLoginState.subscribe((res) => {
      const user = this.loginService.getLoggedInUser();
      if (user !== undefined && user !== null) {
        this.loggedInUserName = user.userName;
      } else {
        this.loggedInUserName = '###';
      }
    });
  }
}
