import { TestBed } from '@angular/core/testing';

import { ContentMenuService } from './content-menu.service';

describe('ContentMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContentMenuService = TestBed.get(ContentMenuService);
    expect(service).toBeTruthy();
  });
});
