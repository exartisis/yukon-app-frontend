import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { UserRegistrationModule } from '../user-registration/user-registration.module';
import { UserRegistrationComponent } from '../user-registration/pages/user-registration/user-registration.component';
import { ContentMenuComponent } from './components/content-menu/content-menu.component';
import { ContentMenuService } from './services/content-menu/content-menu.service';

@NgModule({
  declarations: [HomeComponent, ContentMenuComponent],
  imports: [
    CommonModule,
    UserRegistrationModule
  ],
  exports: [
    HomeComponent
  ],
  providers: [
    ContentMenuService
  ],
  entryComponents: [
    UserRegistrationComponent
  ]
})
export class HomeModule { }
