import { Component, OnInit, OnDestroy } from '@angular/core';
import { Users } from '../../../../core/api/common/models/users';
import { ApiService } from '../../../../core/services/api/api.service';
import {UserRegistrationService} from '../../services/user-registration/user-registration.service';
import {APICRUDController, CRUD_ACTION} from '../../../../core/api/common/apicrudcontroller';
import {Error} from 'tslint/lib/error';
import {ToastService} from '../../../../core/services/toast/toast.service';

/**
 * Users registration form.
 * @author Roshan Gerard Bolonna
 */
@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit, OnDestroy, APICRUDController {

  protected userList: Array<Users> = new Array<Users>();

  protected userModel: Users = new  Users();
  protected txtUserPswConfirm: string;

  // ****** References to subscriptions. ****** //
  private subUserRecSelected: any;

  constructor(private apiService: ApiService,
              private userRegService: UserRegistrationService,
              private toastService: ToastService) {}

  ngOnInit() {
    this.initServiceSubscriptions();
    this.initSettings();
  }

  ngOnDestroy() {
    this.subUserRecSelected.unsubscribe();
  }

  initSettings() {
    this.loadUsersList();
  }

  initServiceSubscriptions() {
    this.subUserRecSelected = this.userRegService.subUserRecSelected.subscribe(result => {
      this.userModel = result;
      this.txtUserPswConfirm = this.userModel.password;
    });
  }

  // ****** Other form functions. ****** //

  /**
   * Load users list.
   */
  loadUsersList() {
    const subRef = this.apiService.getUsers().subscribe((res) => {
      try {
        this.userList = res;
      } finally {
        subRef.unsubscribe();
      }
    });
  }

  /**
   * Reset form fields.
   */
  resetForm() {
    this.userModel = new Users();

    // Other fields.
    this.txtUserPswConfirm = '';

    // Other API actions.
    this.loadUsersList();
  }

  /**
   * Clean specific tasks REST call.
   * @param subRef
   */
  cleanTask(subRef) {
    subRef.unsubscribe();
  }

  /**
   * Validate input data consistency prior to allow any DML CRUD operations.
   */
  validateConsistency(validateAction: CRUD_ACTION) {
    if (validateAction === CRUD_ACTION.ACTION_DELETE || validateAction === CRUD_ACTION.ACTION_UPDATE) {
      if (this.userModel.userID === -1) {
        this.toastService.showError('Invalid Selection', 'Select an user record first.');
        return false;
      }
    }

    // Verify essential fields were filled.
    if (validateAction === CRUD_ACTION.ACTION_SAVE || validateAction === CRUD_ACTION.ACTION_UPDATE) {
      if ((this.userModel.firstName as any).length <= 0) {
        this.toastService.showError('Incomplete', 'First name not specified.');
        return false;
      }

      if ((this.userModel.lastName as any).length <= 0) {
        this.toastService.showError('Incomplete', 'Last name not specified.');
        return false;
      }

      if ((this.userModel.userName as any).length <= 0) {
        this.toastService.showError('Incomplete', 'User name not specified or invalid.');
        return false;
      }

      if ((this.userModel.password as any).length <= 0 || (this.userModel.password !== this.txtUserPswConfirm)) {
        this.toastService.showError('Incomplete or Invalid', 'Please confirm password.');
        return false;
      }
    }

    return true;
  }

  // ****** CRUD Operations. ****** //

  /**
   * Create new user.
   */
  save() {
    return new Promise((resolve: any, reject) => {
      const subRef = this.apiService.createUser(this.userModel).subscribe((result: number) => {
        if (result > -1) {
          console.log('New user: ' + result);
          resolve({subRef, result});
        } else {
          console.log('Error..!');
          reject(result);
        }
      });
    });
  }

  /**
   * Update record.
   */
  update() {
    return new Promise((resolve: any, reject) => {
      const subRef = this.apiService.updateUser(this.userModel).subscribe((result: number) => {
        if (result > 0) {
          console.log('Updated: ' + result);
          resolve({subRef, result});
        } else {
          console.log('Error...!');
          reject(result);
        }
      });
    });
  }

  /**
   * Delete record.
   */
  delete() {
    return new Promise((resolve: any, reject) => {
      const subRef = this.apiService.deleteUser(this.userModel.userID).subscribe((result: number) => {
        if (result > 0) {
          console.log('Deleted: ' + result);
          resolve({subRef, result});
        } else {
          console.log('Error...!');
          reject(result);
        }
      });
    });
  }

  commit(action: CRUD_ACTION) {
    switch (action) {
      case CRUD_ACTION.ACTION_SAVE:
        if (this.validateConsistency(action)) {
          this.save().then((val: any) => {
            this.toastService.showSuccess('Submitted', 'User record saved: ' + val.result);
            this.cleanTask(val.subRef);
            this.resetForm();
          }).catch(
            (err => this.toastService.showError('Error', err))
          );
        }
        break;

      case CRUD_ACTION.ACTION_UPDATE:
        if (this.validateConsistency(action)) {
          this.update().then((val: any) => {
            this.toastService.showSuccess('Updated', 'Record updated (affected rows): ' + val.result);
            this.cleanTask(val.subRef);
            this.resetForm();
          }).catch(
            (err => this.toastService.showError('Error', err))
          );
        }
        break;

      case CRUD_ACTION.ACTION_DELETE:
        if (this.validateConsistency(action)) {
          this.delete().then((val: any) => {
            this.toastService.showWarning('Critical Action',
              `A user record deleted -> UserID: ${this.userModel.userID}, Affected Rows: ${val.result}`);
            this.resetForm();
          }).catch(
            (err => this.toastService.showError('Error', err))
          );
        }
        break;

      default:
        throw new Error('UNKNOWN CRUD ACTION..!');
    }
  }

  // ****** Events. ****** //

  /**
   * Triggered when user id record item was clicked.
   * @param userID - user ID to pass.
   */
  onUserRecClicked(userID: number) {
    const subRef = this.apiService.getUserByID(userID).subscribe((result: Users) => {
      try {
        this.userRegService.emitUserRecSelected(result);
      } finally {
        subRef.unsubscribe();
      }
    });
  }

  /**
   * Reset form.
   */
  onResetClicked() {
    this.resetForm();
  }

  /**
   * Save record.
   */
  onSaveClicked() {
    this.commit(CRUD_ACTION.ACTION_SAVE);
  }

  /**
   * Update record.
   */
  onUpdateClicked() {
    this.commit(CRUD_ACTION.ACTION_UPDATE);
  }

  /**
   * Delete record.
   */
  onDeleteClicked() {
    this.commit(CRUD_ACTION.ACTION_DELETE);
  }
}
