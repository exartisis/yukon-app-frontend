import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRegistrationComponent } from './pages/user-registration/user-registration.component';
import { CoreModule } from '../../core/core.module';
import { UserRegistrationService } from './services/user-registration/user-registration.service';

@NgModule({
  declarations: [UserRegistrationComponent],
  imports: [
    CommonModule,
    CoreModule
  ],
  exports: [
    UserRegistrationComponent
  ],
  providers: [
    UserRegistrationService
  ]
})
export class UserRegistrationModule { }
