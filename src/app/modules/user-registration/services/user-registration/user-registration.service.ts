import {EventEmitter, Injectable, Output} from '@angular/core';
import {Users} from '../../../../core/api/common/models/users';

@Injectable()
export class UserRegistrationService {

  /**
   * Broadcast listener: selected users details.
   * @type {EventEmitter<Users>}
   */
  @Output() subUserRecSelected: EventEmitter<Users> = new EventEmitter<Users>();

  /**
   * Broadcast selected user details.
   * @param {Users} user
   */
  emitUserRecSelected(user: Users) {
    this.subUserRecSelected.emit(user);
  }
}
