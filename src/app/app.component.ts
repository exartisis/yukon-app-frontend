import { Component, OnInit, AfterContentInit } from '@angular/core';
import {LoginService} from './core/services/auth/login.service';
import {LoginComponent} from './core/auth/components/login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'yukon-app-frontend';

  constructor(private loginService: LoginService) {}

  ngOnInit() {}
}
