import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CoreModule} from './core/core.module';
import {HomeModule} from './modules/home/home.module';
import {ComponentRouterService} from './core/services/home/component-router.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ApiService} from './core/services/api/api.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    CoreModule,
    HomeModule
  ],
  providers: [ComponentRouterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
