import { Component, OnInit } from '@angular/core';
import {LoginService} from '../services/auth/login.service';

/**
 * Main global header of the application.
 * @author Roshan Gerard Bolonna
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  ngOnInit() {}

  // ****** Events. ****** //

  btnLogoutClicked() {
    this.loginService.open();
    this.loginService.emitLoginState(false);
  }
}
