import {Component, EventEmitter, Injectable, Output} from '@angular/core';
import {LoginComponent} from '../../auth/components/login/login.component';
import {Users} from '../../api/common/models/users';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  @Output() subLoginState: EventEmitter<boolean> = new EventEmitter<boolean>();

  private modal: LoginComponent;
  private user: Users;

  // Application wide broadcasting.

  /**
   * Broadcast login state.
   * @param {boolean} loginState - current login state.
   */
  emitLoginState(loginState: boolean) {
    if (loginState === false) {
      this.setLoggedInUser(null);
    }

    this.subLoginState.emit(loginState);
  }

  // Component wise communication.

  /**
   * Set component to render as a modal dialog.
   * @param {LoginComponent} modal - component referenc
   */
  setComponent(modal: LoginComponent) {
    this.modal = modal;
  }

  /**
   * Set logged in user.
   * @param {Users} user - specify user model.
   */
  setLoggedInUser(user: Users) {
    this.user = user;
  }

  /**
   * Retrieve currently logged in user.
   * @return {Users} - return currently logged in user.
   */
  getLoggedInUser(): Users {
    return this.user;
  }

  /**
   * Open modal dialog.
   */
  open() {
    this.modal.open();
  }

  /**
   * Close modal dialog.
   */
  close() {
    this.modal.close();
  }
}
