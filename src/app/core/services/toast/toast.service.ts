import { Injectable } from '@angular/core';
import {ToastrService} from 'ngx-toastr';

/**
 * Common notifications management service.
 * @author Roshan Gerard Bolonna
 */
@Injectable()
export class ToastService {

  constructor(private toastrService: ToastrService) { }

  /**
   *  Show success message.
   */
  showSuccess(title: string, msg: string) {
    this.toastrService.success(title, msg);
  }

  /**
   * Show error message.
   * @param {string} title
   * @param {string} msg
   */
  showError(title: string, msg: string) {
    this.toastrService.error(msg, title);
  }

  /**
   * Show warning for cirtical actions.
   * @param {string} title
   * @param {string} msg
   */
  showWarning(title: string, msg: string) {
    this.toastrService.warning(msg, title);
  }
}
