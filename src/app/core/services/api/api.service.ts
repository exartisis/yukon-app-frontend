import { Injectable } from '@angular/core';
import {Users} from '../../api/common/models/users';
import {HttpClient} from '@angular/common/http';
/**
 * API service for DB CRUD operations.
 * @author Roshan Gerard Bolonna
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiURL = 'http://localhost:9001';

  constructor(private httpClient: HttpClient) {}

  // ****** User specific service actions. ****** //

  /**
   * Create an user.
   * @param {Users} user - data model to request.
   * @return {Observable<Object>} - Handle returned response.
   */
  public createUser(user: Users) {
    return this.httpClient.post(`${this.apiURL}/api/createUser`, user);
  }

  /**
   * Update an user with new details.
   * @param {Users} user - data model to request.
   * @return {Observable<Object>} - Handle returned response.
   */
  public updateUser(user: Users) {
    return this.httpClient.put(`${this.apiURL}/api/updateUser`, user);
  }

  /**
   * Delete user specified by the ID.
   * @param {number} userID
   * @return {Observable<Object>} - Handle returned response.
   */
  public deleteUser(userID: number) {
    return this.httpClient.delete(`${this.apiURL}/api/deleteUser/${userID}`);
  }

  /**
   * Get user by id
   * @param {number} userID - ID of an user record to retrieve.
   * @return {Observable<Object>} - Handle returned response.
   */
  public getUserByID(userID: number) {
    return this.httpClient.get(`${this.apiURL}/api/users/${userID}`);
  }

  /**
   * Get all user records.
   * @return {Observable<Users[]>} - Handle returned response.
   */
  public getUsers() {
    return this.httpClient.get<Users[]>(`${this.apiURL}/api/users`);
  }

  /**
   * Get authenticated user details.
   * @return {Observable<Users>} - Return details to determine user was granted the access.
   */
  public getUserLogin(user: Users) {
    return this.httpClient.post<Users>(`${this.apiURL}/api/loginUser`, user);
  }
}
