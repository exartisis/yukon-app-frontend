import {ComponentFactoryResolver, ComponentRef, Inject, Injectable, Injector, ViewContainerRef} from '@angular/core';
import {UserRegistrationComponent} from '../../../modules/user-registration/pages/user-registration/user-registration.component';

/**
 * Component router service which responsible for rendering instance or static based components.
 * @author Roshan Gerard Bolonna
 */
@Injectable()
export class ComponentRouterService {

  private factoryResolver: ComponentFactoryResolver;
  private rootViewContainer: ViewContainerRef;
  private currentActiveComponent: ComponentRef<any>;

  // Reusable components.
  private readonly compUserRegistration: ComponentRef<UserRegistrationComponent>;

  constructor(
    @Inject(ComponentFactoryResolver) factoryResolver,
    private injector: Injector,
  ) {
    this.factoryResolver = factoryResolver;

    // Initialize reusable components.
    this.compUserRegistration = this.factoryResolver.resolveComponentFactory(UserRegistrationComponent).create(this.injector);
  }

  /**
   * Set root view container to render components dynamically.
   * @param viewContainerRef - refernece to rendering area,
   */
  setRootViewContainer(viewContainerRef) {
    this.rootViewContainer = viewContainerRef;
  }

  /**
   * Render user registration.
   * @param state - "false" dispose previous, "true" dispose current.
   */
  renderUserRegistration(state: boolean) {
    if (state === false) {
      if (this.rootViewContainer != null && this.currentActiveComponent != null) {
        this.rootViewContainer.detach(this.rootViewContainer.indexOf(this.currentActiveComponent.hostView));
      }
    } else if (this.currentActiveComponent !== undefined && this.currentActiveComponent !== null)  {
      this.rootViewContainer.detach(this.rootViewContainer.indexOf(this.currentActiveComponent.hostView));
      return;
    } else {
      return;
    }

    this.updateRenderer(this.compUserRegistration);
  }

  /**
   * Update renderer with new rendering context.
   * @param {ComponentRef<any>} rendererRef - Reference to context which need to be rendered.
   */
  updateRenderer(rendererRef: ComponentRef<any>) {
    this.currentActiveComponent = rendererRef;
    this.rootViewContainer.insert(rendererRef.hostView);
  }
}
