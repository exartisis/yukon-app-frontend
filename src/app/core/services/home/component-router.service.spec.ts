import { TestBed } from '@angular/core/testing';

import { ComponentRouterService } from './component-router.service';

describe('ComponentRouterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComponentRouterService = TestBed.get(ComponentRouterService);
    expect(service).toBeTruthy();
  });
});
