import {Component, ElementRef, Input, OnInit, OnDestroy, AfterContentInit, ViewEncapsulation} from '@angular/core';
import {LoginService} from '../../../services/auth/login.service';
import {Users} from '../../../api/common/models/users';
import {ApiService} from '../../../services/api/api.service';

/**
 * Login and authentication component/
 * @author Roshan Gerard Bolonna
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy, AfterContentInit {

  private readonly element: any;
  protected usersModel: Users = new Users();
  protected alertInvalidLogin = 'login-alert-invalid-hide';

  constructor(private loginService: LoginService, private apiService: ApiService, private elm: ElementRef) {
    this.element = elm.nativeElement;
  }

  ngOnInit(): void {
    // Append to end of page in order to display on top of the content.
    document.body.appendChild(this.element);

    // Prevent from closing the modal when click on background.
    this.element.addEventListener('click', (e: any) => {
      return;
    });

    // Specify current component as the modal component.
    this.loginService.setComponent(this);
  }

  ngOnDestroy() {
    this.element.remove();
  }

  ngAfterContentInit() {
    this.loginService.open();
  }

  /**
   * Open modal.
   */
  open(): void {
    this.element.style.display = 'block';
    document.body.classList.add('login-modal-open');
  }

  /**
   * Close modal.
   */
  close(): void {
    this.element.style.display = 'none';
    document.body.classList.remove('login-modal-open');
  }

  // ****** Events. ****** //

  /**
   * Login button event.
   */
  onBtnLoginClicked() {
    this.apiService.getUserLogin(this.usersModel).subscribe((res) => {
      if ((res as any).length === 0) {
        this.alertInvalidLogin = 'login-alert-invalid-show';
        this.loginService.setLoggedInUser(null);
        this.loginService.emitLoginState(false);
        return;
      }

      this.alertInvalidLogin = 'login-alert-invalid-hide';
      this.loginService.close();
      this.loginService.setLoggedInUser(res[0]);
      this.loginService.emitLoginState(true);
    });
  }

  /**
   * Login close button event
   */
  onBtnCloseClicked() {
    this.loginService.close();
    this.loginService.setLoggedInUser(null);
    this.loginService.emitLoginState(false);
  }
}
