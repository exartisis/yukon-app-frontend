import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import {ComponentRouterService} from './services/home/component-router.service';
import {LoginComponent} from './auth/components/login/login.component';
import {LoginService} from './services/auth/login.service';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ApiService} from './services/api/api.service';
import {ToastService} from './services/toast/toast.service';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [FooterComponent, HeaderComponent, LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    FormsModule
  ],
  providers: [
    ComponentRouterService,
    LoginService,
    ApiService,
    ToastService
  ]
})
export class CoreModule { }
