export enum CRUD_ACTION {
  ACTION_SAVE,
  ACTION_UPDATE,
  ACTION_DELETE
}

export interface APICRUDController {
  save();
  update();
  delete();

  commit(action: CRUD_ACTION);
}
