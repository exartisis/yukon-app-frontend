/**
 * Representing model for user data received.
 * @author Roshan Gerard Bolonna
 */
export class Users {
  userID: number = -1;
  firstName: string = '';
  lastName: string = '';
  userName: string = '';
  password: string = '';
}
